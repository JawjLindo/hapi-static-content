# Hapi Static Content Plugin
For delivery of static content in a Hapi.js application

Example Usage:

```javascript
var Hapi = require('hapi');
var server = new Hapi.Server();
server.connection({ host: 'localhost' });

var options = {
    path: './public' // to make the 'public' directory the path statically shared 
};

server.register({
    register: require('hapi-static-content'),
    options: options
}, function (err) {

    if (err) {
        console.error(err);
    }
    else {
        server.start(function () {

            console.info('Server started at ' + server.info.uri);
        });
    }
});
```